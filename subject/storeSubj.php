<?php

require_once '../classes/Subject.php';

if (!empty($_POST['name']))
{
  foreach ($_POST as $key => $value)
  {
    $_POST[$key] = htmlspecialchars($value);
  }

  $subject = new Subject($_POST['name']);
  $subject->store();

  header('location:index.php');
}

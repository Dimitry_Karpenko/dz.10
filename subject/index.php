<?php

require_once '../classes/Subject.php';
require_once '../config/db.php';

$subjectObjs = Subject::all($pdo);

?>

<!doctype html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Список предметов</title>
  <style>
    th,td
    {
      border: 1px solid black;
    }
    table
    {
      border-collapse: collapse;
    }
  </style>
</head>
<body>
<a href="../index.php">На главную</a>
<h1> Список предметом</h1>

<table>
  <tr>
    <th>
      Предмет
    </th>
    <th>
      Преподаватели
    </th>
    <th>
      Редактировать
    </th>
    <th>
      Удалить
  </tr>

  <?php foreach ($subjectObjs as $subject):?>
    <tr>
      <td>
        <?=$subject->getName()?>
      </td>
      <td>
        <?php foreach ($subject->getProfessors() as $prof):?>
          <?=$prof->getName();?> <?=$prof->getSurname();?> <br>
        <?php endforeach;?>
      </td>
      <td>
        <a href="edit.php?id=<?=$subject->getId()?>">Редактировать</a>
      </td>
      <td>
        <form action="delete.php" method="post">
          <input type="hidden" name="id" value="<?=$subject->getId()?>">
          <input type="submit" name="submit" value="Удалить">
        </form>
    </tr>
  <?php endforeach;?>
</table>

<h2>Добавить предмет</h2>

<form action="storeSubj.php" method="post">
  <div>
    <label for="">
      <input type="text" name="name">
    </label>
  </div>

  <div>
    <input type="submit" name="submit">
  </div>
</form>

</body>
</html>

<?php

require_once '../config/db.php';
require_once '../classes/Subject.php';

if (empty($_GET['id'])) header('location:index.php');

$subject = Subject::getById($pdo, $_GET['id']);

?>

<!doctype html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Редактировать предмет</title>
</head>
<body>

<h1>Редактировать предмет</h1>

<form action="update.php" method="post">
  <input type="hidden" name="id" value="<?=$subject->getId()?>">
  <div>
    <label for="">
      Название предмета
      <input type="text" name="name" value="<?=$subject->getName()?>">
    </label>
  </div>

  <div>
    <input type="submit" name="submit">
  </div>


</form>

</body>
</html>

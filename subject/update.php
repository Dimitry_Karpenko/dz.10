<?php
require_once '../classes/Subject.php';
require_once '../config/db.php';

if (isset($_POST['submit']))
{
  foreach ($_POST as $key => $value)
  {
    $_POST[$key] = htmlspecialchars($value);
  }

  Subject::edit($pdo, $_POST['id'], $_POST['name']);

  header('location:index.php');

}
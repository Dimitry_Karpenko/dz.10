<?php

require_once '../classes/Department.php';

if (!empty($_POST['name']))
{
  foreach ($_POST as $key => $value)
  {
    $_POST[$key] = htmlspecialchars($value);
  }

  $departemt = new Department($_POST['name'], $_POST['phone']);
  $departemt->store();

  header('location:index.php');

}

<?php
require_once '../config/db.php';
require_once '../classes/Department.php';

$departmentObjs = Department::all($pdo);

?>

<!doctype html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Кафедры</title>
</head>
<body>
<a href="../index.php">На главную</a>
<h1>Кафедры</h1>

<table>
  <tr>
    <th>
      Кафедра
    </th>
    <th>
      Телефон
    </th>
    <th>
      Редактировать
    </th>
    <th>
      Удалить
    </th>
  </tr>

  <?php foreach ($departmentObjs as $depatment):?>
    <tr>
      <td>
        <?=$depatment->getName()?>
      </td>
      <td>
        <?=$depatment->getPhone()?>
      </td>
      <td>
        <a href="edit.php?id=<?=$depatment->getId()?>">Редактировать</a>
      </td>
      <td>
        <form action="delete.php" method="post">
          <input type="hidden" name="id" value="<?=$depatment->getId()?>">
          <input type="submit" name="submit" value="Удалить">
        </form>
      </td>
    </tr>
  <?php endforeach;?>
</table>

<h2>Добавить кафедру</h2>

<form action="storeDept.php" method="post">
  <div>
    <label for="">
      Название кафедры
      <input type="text" name="name">
    </label>
  </div>
  <div>
    <label for="">
      Телефон кафедры
      <input type="text" name="phone">
    </label>
  </div>
  <div>
    <input type="submit" name="submit">
  </div>
</form>

</body>
</html>

<?php

require_once '../config/db.php';
require_once '../classes/Department.php';

if (isset($_POST['submit']))
{
  foreach ($_POST as $key => $value)
  {
    $_POST[$key] = htmlspecialchars($value);
  }


  Department::edit($_POST['id'], $_POST['name'], $_POST['phone']);

  header('location:index.php');
}
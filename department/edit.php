<?php

require_once '../config/db.php';
require_once '../classes/Department.php';

if (!empty($_GET['id']))
{
  foreach ($_POST as $key => $value)
  {
    $_POST[$key] = htmlspecialchars($value);
  }

  $departmentObj = Department::getById($pdo, $_GET['id']);

}


?>

<!doctype html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Редактировать кафедру</title>
</head>
<body>
<h1>Редактировать кафедру</h1>
<form action="update.php" method="post">
  <div>
    <input type="hidden" name="id" value="<?=$departmentObj->getId()?>">
    <label for="">
      Название кафедры
      <input type="text" name="name" value="<?=$departmentObj->getName()?>">
    </label>
  </div>
  <div>
    <label for="">
      Телефон кафедры
      <input type="text" name="phone" value="<?=$departmentObj->getPhone()?>">
    </label>
  </div>
  <div>
    <input type="submit" name="submit">
  </div>
</form>

</body>
</html>

<?php

require_once 'Db.php';
require_once 'Department.php';
require_once 'Subject.php';


class Profesor extends Db
{

  protected $id;
  protected $name;
  protected $surname;
  protected $email;
  protected $dept_id;
  protected $subjId;

  public function __construct($name, $surname, $email, $dept_id)
  {
    parent::__construct();
    $this->name = $name;
    $this->surname = $surname;
    $this->email = $email;
    $this->dept_id = $dept_id;
  }

  /**
   * @return mixed
   */
  public function getSubjId()
  {
    return $this->subjId;
  }

  public function setSubjId($subjId)
  {
    $this->subjId = $subjId;
  }

  /**
   * @param mixed $id
   */
  public function setId($id)
  {
    $this->id = $id;
  }

  /**
   * @return mixed
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * @return mixed
   */
  public function getName()
  {
    return $this->name;
  }

  /**
   * @return mixed
   */
  public function getSurname()
  {
    return $this->surname;
  }

  /**
   * @return mixed
   */
  public function getEmail()
  {
    return $this->email;
  }

  /**
   * @return mixed
   */
  public function getDeptId()
  {
    return $this->dept_id;
  }

  public function store()
  {
    try
    {
      $this->dept_id = (int) $this->dept_id;

      $sql = 'INSERT INTO profesors SET
            name = :name,
            surname = :surname,
            email = :email,
            dept_id = :dept_id                      
           ';
      $sth = $this->pdo->prepare($sql);
      $sth->bindValue(':name', $this->name);
      $sth->bindValue(':surname', $this->surname);
      $sth->bindValue(':email', $this->email);
      $sth->bindValue(':dept_id', $this->dept_id);
      $sth->execute();
      $this->setId($this->pdo->lastInsertId());

      $sql = 'INSERT INTO prof_subj SET
              prof_id = :prof_id,
              subj_id = :subj_id
             ';
      $sth = $this->pdo->prepare($sql);


      foreach ($this->subjId as $subj)
      {
        $sth->bindValue(':subj_id', $subj);
        $sth->bindValue(':prof_id', $this->id);
        $sth->execute();
      }
    }
    catch (Exception $exception)
    {
      echo 'Ошибка заполнения таблиц БД! Код: '.$exception->getCode().' Сообщение: '.$exception->getMessage();
    }

  }

  public function getDept()
  {
    try
    {
      $sql = 'SELECT * FROM departments WHERE id = :id';
      $sth = $this->pdo->prepare($sql);
      $sth->bindValue(':id', $this->dept_id);
      $sth->execute();
      $deptArr = $sth->fetchAll();
      $deptObj = new Department($deptArr[0]['name'], $deptArr[0]['phone']);
      $deptObj->setId($deptArr[0]['id']);

      return $deptObj;
    }
    catch (Exception $exception)
    {
      echo 'Ошибка получения данных из БД! Код: '.$exception->getCode().' Сообщение: '.$exception->getMessage();
    }
  }


  static public function all(PDO $pdo)
  {
    $sql = 'SELECT * FROM profesors';
    $sth = $pdo->query($sql);
    $profesorsArr = $sth->fetchAll();

    $profesorsObjs = [];

    foreach ($profesorsArr as $profesor)
    {
      $profesorObj = new self($profesor['name'], $profesor['surname'], $profesor['email'], $profesor['dept_id']);
      $profesorObj->setId($profesor['id']);

      $profesorsObjs[] = $profesorObj;
    }

    return $profesorsObjs;
  }

  public function getSubjects()
  {
    $sql = '
            SELECT subj_id, name FROM prof_subj as ps
            INNER JOIN subjects as s 
            on ps.subj_id = s.id
            WHERE ps.prof_id = :id
           ';
    $sth = $this->pdo->prepare($sql);
    $sth->bindValue(':id', $this->id);
    $sth->execute();
    $subjArr = $sth->fetchAll();
    $subjObjs = [];
    foreach ($subjArr as $subj)
    {
      $subjObj = new Subject($subj['name']);
      $subjObj->setId($subj['id']);
      $subjObjs[] = $subjObj;
    }

    return $subjObjs;
  }

}
<?php


class Db
{

  protected $pdo;
  private $dbParams;

  public function __construct()
  {
    try
    {
      $this->dbParams = [
          'host' => 'localhost',
          'dbname' => 'school',
          'user' => 'root',
          'pass' => ''
      ];

      $dsn = "mysql:host={$this->dbParams['host']};dbname={$this->dbParams['dbname']}";

      $this->pdo = new PDO($dsn, $this->dbParams['user'], $this->dbParams['pass']);

      $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

      $this->pdo->exec('SET NAMES "utf8"');
    }
    catch (Exception $exception)
    {
      echo 'Ошибка подключения к БД! Код: '.$exception->getCode().' Сообщение: '.$exception->getMessage();
    }


  }

  public function getPdo()
  {
    return $this->pdo;
  }



}
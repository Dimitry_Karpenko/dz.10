<?php

require_once 'Db.php';
require_once 'Profesor.php';

class Subject extends Db
{
  protected $id;
  protected $name;

  public function __construct($name)
  {
    parent::__construct();
    $this->name = $name;
  }

  public function getId()
  {
    return $this->id;
  }


  public function setId($id)
  {
    $this->id = $id;
  }

  public function getName()
  {
    return $this->name;
  }

  public function store()
  {
    try
    {
      $sql = 'INSERT INTO subjects SET
              name = :name 
             ';
      $sth = $this->pdo->prepare($sql);
      $sth->bindValue(':name', $this->name);
      $sth->execute();
    }
    catch (Exception $exception)
    {
      echo 'Ошибка заполнения таблиц БД! Код: '.$exception->getCode().' Сообщение: '.$exception->getMessage();
    }

  }

  static public function all($pdo)
  {
    try
    {
      $sql = 'SELECT * FROM subjects';
      $sth = $pdo->query($sql);
      $subjArr = $sth->fetchAll();

      $subjObjs = [];

      foreach ($subjArr as $subj)
      {
        $subjObj = new self($subj['name']);
        $subjObj->setId($subj['id']);
        $subjObjs[] = $subjObj;
      }

      return $subjObjs;
    }
    catch (Exception $exception)
    {
      echo 'Ошибка получения данных из БД! Код: '.$exception->getCode().' Сообщение: '.$exception->getMessage();
    }

  }

  static public function getById(PDO $pdo, $id)
  {
    try
    {
      $sql = 'SELECT * FROM subjects
            WHERE id = :id
           ';
      $sth = $pdo->prepare($sql);
      $sth->bindValue(':id', $id);
      $sth->execute();
      $subjArr = $sth->fetchAll();

      $subjObj = new self($subjArr[0]['name']);
      $subjObj->setId($subjArr[0]['id']);

      return $subjObj;

    }
    catch (Exception $exception)
    {
      echo 'Ошибка получения данных из БД! Код: '.$exception->getCode().' Сообщение: '.$exception->getMessage();
    }



  }

  static public function edit(PDO $pdo, $id, $name)
  {
    $subjObj = new self($name);
    $subjObj->setId($id);
    $subjObj->update($pdo);
  }

  public function update()
  {
    try
    {
      $sql = 'UPDATE subjects SET
            name = :name 
            WHERE id = :id
            ';
      $sth = $this->pdo->prepare($sql);
      $sth->bindValue(':name', $this->name);
      $sth->bindValue(':id', $this->id);
      $sth->execute();
    }
    catch (Exception $exception)
    {
      echo 'Ошибка обновления данных из БД! Код: '.$exception->getCode().' Сообщение: '.$exception->getMessage();
    }
  }

  static public function delete(PDO $pdo, $id)
  {
    $subjectObj = self::getById($pdo, $id);
    $subjectObj->destroy();
  }

  public function destroy()
  {
    try
    {
      $sql = 'DELETE FROM subjects WHERE id = :id ';
      $sth = $this->pdo->prepare($sql);
      $sth->bindValue(':id', $this->id);
      $sth->execute();
    }
    catch (Exception $exception)
    {
      echo 'Ошибка удаления данных из БД! Код: ' . $exception->getCode() . ' Сообщение: ' . $exception->getMessage();
    }
  }

  public function getProfessors()
  {
    $sql = '
            SELECT p.id, p.name, p.surname, p.email, p.dept_id FROM prof_subj as ps
            INNER JOIN profesors as p
            on ps.prof_id = p.id
            WHERE ps.subj_id = :id
           ';
    $sth = $this->pdo->prepare($sql);
    $sth->bindValue(':id', $this->id);
    $sth->execute();
    $profArr = $sth->fetchAll();

    $profObjs = [];

    foreach ($profArr as $prof)
    {
      $profObj = new Profesor($prof['name'], $prof['surname'], $prof['email'], $prof['dept_id']);
      $profObj->setId($prof['id']);

      $profObjs[] = $profObj;
    }

    return $profObjs;
  }


}
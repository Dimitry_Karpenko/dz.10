<?php

require_once 'Db.php';

class Department extends Db
{

  protected $id;
  protected $name;
  protected $phone;

  public function __construct($name, $phone)
  {
    parent::__construct();
    $this->name = $name;
    $this->phone = $phone;
  }

  public function setId($id)
  {
    $this->id = $id;
  }

  public function getId()
  {
    return $this->id;
  }

  public function getName()
  {
    return $this->name;
  }

  public function getPhone()
  {
    return $this->phone;
  }

  public function store()
  {
    try {
      $sql = 'INSERT INTO departments SET
             name = :name,
             phone = :phone
            ';
      $sth = $this->pdo->prepare($sql);
      $sth->bindValue(':name', $this->name);
      $sth->bindValue(':phone', $this->phone);
      $sth->execute();
    } catch (Exception $exception) {
      echo 'Ошибка заполнения таблиц БД! Код: ' . $exception->getCode() . ' Сообщение: ' . $exception->getMessage();
    }
  }

  static public function all($pdo)
  {
    try {
      $sql = 'SELECT * FROM departments';
      $sth = $pdo->query($sql);
      $deptArr = $sth->fetchAll();
      $deptObjs = [];
      foreach ($deptArr as $dept) {
        $deptObject = new self($dept['name'], $dept['phone']);
        $deptObject->setId($dept['id']);
        $deptObjs[] = $deptObject;

      }

      return $deptObjs;

    }
    catch (Exception $exception)
    {
      echo 'Ошибка получения данных из БД! Код: ' . $exception->getCode() . ' Сообщение: ' . $exception->getMessage();
    }


  }

  static public function getById(PDO $pdo, $id)
  {
    try
    {
      $sql = 'SELECT * FROM departments WHERE id = :id';
      $sth = $pdo->prepare($sql);
      $sth->bindValue(':id', $id);
      $sth->execute();
      $deptArr = $sth->fetchAll();

      $deptObj = new self($deptArr[0]['name'], $deptArr[0]['phone']);
      $deptObj->setId($deptArr[0]['id']);

      return $deptObj;
    }
    catch (Exception $exception)
    {
      echo 'Ошибка получения данных из БД! Код: ' . $exception->getCode() . ' Сообщение: ' . $exception->getMessage();
    }



  }

  static public function edit($id, $name, $phone)
  {
    $dept = new self($name, $phone);
    $dept->setId($id);
    $dept->update();

  }

  public function update()
  {
    try
    {
      $sql = 'UPDATE departments SET
            name = :name,
            phone = :phone
            WHERE id = :id
            ';
      $sth = $this->pdo->prepare($sql);
      $sth->bindValue(':id', $this->id);
      $sth->bindValue(':name', $this->name);
      $sth->bindValue(':phone', $this->phone);
      $sth->execute();
    }
    catch (Exception $exception)
    {
      echo 'Ошибка обновления данных в БД! Код: ' . $exception->getCode() . ' Сообщение: ' . $exception->getMessage();
    }

  }

  static public function delete(PDO $pdo, $id)
  {
    $dept = self::getById($pdo, $id);
    $dept->destroy();
  }

  public function destroy()
  {
    try
    {
      $sql = 'DELETE FROM departments WHERE id = :id';
      $sth = $this->pdo->prepare($sql);
      $sth->bindValue(':id', $this->id);
      $sth->execute();
    }
    catch (Exception $exception)
    {
      echo 'Ошибка удаления данных из БД! Код: ' . $exception->getCode() . ' Сообщение: ' . $exception->getMessage();
    }

  }

}


<?php



try
{
  $dbParams = [
      'host' => 'localhost',
      'dbname' => 'school',
      'user' => 'root',
      'pass' => ''
  ];


  $dsn = "mysql:host={$dbParams['host']};dbname={$dbParams['dbname']}";

  $pdo = new PDO($dsn, $dbParams['user'], $dbParams['pass']);

  $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

  $pdo->exec('SET NAMES "utf8"');
}

catch (Exception $exception)
{
  echo 'Ошибка подключения к БД! Код: '.$exception->getCode().' Сообщение: '.$exception->getMessage();
}
<?php

require_once '../classes/Db.php';

$db = new Db();
$pdo = $db->getPdo();

try
{
  $sqlDept = 'CREATE TABLE departments
              (
                id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
                name VARCHAR (255),
                phone varchar (255)                
              )
              DEFAULT CHARACTER SET utf8 ENGINE=InnoDB
              ';

  $sqlSubj = 'CREATE TABLE subjects
              (
                id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
                name VARCHAR (255)
              )
              DEFAULT CHARACTER SET utf8 ENGINE=InnoDB
             ';

  $sqlProf = 'CREATE TABLE  profesors
          (
            id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
            name VARCHAR (255),
            surname VARCHAR (255),
            email VARCHAR (255),
            dept_id INT,
            FOREIGN KEY (dept_id) REFERENCES departments (id) 
          )
          DEFAULT CHARACTER SET utf8 ENGINE=InnoDB       
          ';

  $sqlProfSubj = 'CREATE TABLE prof_subj
                  (
                    prof_id INT,
                    subj_id INT,
                    FOREIGN KEY (prof_id) REFERENCES profesors (id) ON DELETE CASCADE,
                    FOREIGN KEY (subj_id) REFERENCES subjects (id) ON DELETE CASCADE                    
                  )
                  DEFAULT CHARACTER SET utf8 ENGINE=InnoDB
                 ';

  $pdo->exec($sqlDept);
  $pdo->exec($sqlSubj);
  $pdo->exec($sqlProf);
  $pdo->exec($sqlProfSubj);

  header('location:index.php');
}

catch (Exception $exception)
{
  echo 'Ошибка создания таблиц в БД! Код: '.$exception->getCode().' Сообщение: '.$exception->getMessage();
}



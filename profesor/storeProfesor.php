<?php

require_once '../classes/Profesor.php';

if (isset($_POST['submit']))
{
  foreach ($_POST as $key => $value)
  {
    if ($_POST['subj_id'])
    {
      $subjId = $_POST['subj_id'];

      foreach ($subjId as $key => $value)
      {
        $subjId[$key] = htmlspecialchars($value);
      }

      $_POST['subj_id'] = $subjId;

      continue;
    }
    $_POST[$key] = htmlspecialchars($value);

  }

  $profesor = new Profesor($_POST['name'], $_POST['surname'], $_POST['email'], $_POST['dept_id']);
  $profesor->setSubjId($_POST['subj_id']);
  $profesor->store();

  header('location:../index.php');
}
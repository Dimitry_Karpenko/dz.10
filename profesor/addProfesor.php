<?php

require_once '../classes/Department.php';
require_once '../classes/Subject.php';
require_once '../config/db.php';

$departments = Department::all($pdo);
$subjects = Subject::all($pdo);

?>

<!doctype html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Добавление преподавателя</title>

</head>
<body>

<h1>Добавление преподавателя</h1>

<form action="storeProfesor.php" method="post">
  <div>
    <label for="">
      Имя
      <input type="text" name="name">
    </label>
  </div>

  <div>
    <label for="">
      Фамилия
      <input type="text" name="surname">
    </label>
  </div>

  <div>
    <label for="">
      Email
      <input type="text" name="email">
    </label>
  </div>

  <div>
    Предмет
    <select name="subj_id[]" id="" multiple>
      <?php foreach ($subjects as $subject):?>
        <option value="<?=$subject->getId()?>"><?=$subject->getName()?></option>
      <?php endforeach;?>
    </select>
  </div>

  <div>
    Кафедра
    <select name="dept_id" id="" >
      <?php foreach ($departments as $department):?>
        <option value="<?=$department->getId()?>"><?=$department->getName()?></option>
      <?php endforeach;?>
    </select>
  </div>

  <div>
    <input type="submit" name="submit">
  </div>

</form>



</body>
</html>


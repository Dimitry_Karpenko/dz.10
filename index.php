<?php
require_once 'classes/Profesor.php';
require_once 'config/db.php';

$profesorObjs = Profesor::all($pdo);

?>
<!doctype html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>
  <style>
    th,td
    {
      border: 1px solid black;
    }
    table
    {
      border-collapse: collapse;
    }
  </style>
</head>
<body>

<div>
  <a href="config/createTables.php">Создать БД</a>
</div>

<div>
  <a href="profesor/addProfesor.php">Добавить преподавателя</a>
</div>

<div>
  <a href="department/">Все кафедры</a>
</div>

<div>
  <a href="subject/">Все предметы</a>
</div>

<table>
  <tr>
    <th>
      Имя
    </th>
    <th>
      Фамилия
    </th>
    <th>
      Кафедра
    </th>
    <th>
      Предметы
    </th>
  </tr>

  <?php foreach ($profesorObjs as $profesor):?>
    <tr>
      <td>
        <?=$profesor->getName()?>
      </td>
      <td>
        <?=$profesor->getSurname()?>
      </td>
      <td>
        <?=$profesor->getDept()->getName()?>
      </td>
      <td>
        <?php foreach ($profesor->getSubjects() as $subj):?>
          <?=$subj->getName()?> <br>
        <?php endforeach;?>
      </td>
    </tr>

  <?php endforeach;?>

</table>





</body>
</html>
